import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:weather_icons/weather_icons.dart';

import '../cred.dart';

class WeatherApp extends StatefulWidget {
  static const String id = "weather_app";
  @override
  _WeatherAppState createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  int temperature = null;
  String condition = "no weather data";
  IconData weatherIcon = WeatherIcons.thermometer_exterior;
  String cityName =
      "searched city. Please serach for a city or tap current location icon.";
  int humidity = 0;
  String sunrise = "00:00";
  String sunset = "00:00";
  String searchCityName = "";
  TextEditingController searchTextController = TextEditingController();
  WeatherModel _weatherModel = WeatherModel();

  @override
  void initState() {
    super.initState();
    initUpdate().then((value) => setState(() {}));
  }

  Future<void> initUpdate() async {
    dynamic weatherData = await WeatherModel().getLocationWeather();
    updateUI(weatherData);
  }

  void updateUI(dynamic weatherData) {
    if (weatherData == null) {
      temperature = null;
      weatherIcon = WeatherIcons.thermometer_exterior;
      condition = "no weather data";
      cityName = "searched city";
      humidity = 0;
      sunrise = "00:00";
      sunset = "00:00";
      return;
    }
    var _t = weatherData["main"]["temp"];
    temperature = _t.toInt();
    condition = weatherData["weather"][0]["description"];
    var _conditionId = weatherData["weather"][0]["id"];
    weatherIcon = _weatherModel.getWeatherIcon(_conditionId);
    cityName = weatherData["name"];
    humidity = weatherData["main"]["humidity"];
    _t = weatherData["sys"]["sunrise"];
    sunrise = _weatherModel.calculateTime(_t);
    _t = weatherData["sys"]["sunset"];
    sunset = _weatherModel.calculateTime(_t);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Weather Man"),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    weatherIcon,
                    size: 68,
                  ),
                  SizedBox(
                    height: 30,
                    width: double.infinity,
                  ),
                  Text(
                    "$temperature°C",
                    style: TextStyle(fontSize: 42, fontWeight: FontWeight.bold),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 80),
                    child: Text(
                      "Currently $condition in $cityName",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 80),
                    child: Text(
                      "Humidity: $humidity%",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 80),
                    child: Text(
                      "Sunrise: $sunrise",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 80),
                    child: Text(
                      "Sunset: $sunset",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Material(
                    child: MaterialButton(
                      color: Colors.deepPurple.shade50,
                      elevation: 2.0,
                      height: 58,
                      minWidth: 58,
                      onPressed: () async {
                        dynamic weatherData =
                            await _weatherModel.getLocationWeather();
                        setState(() {
                          updateUI(weatherData);
                        });
                      },
                      child: Icon(
                        Icons.my_location,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: TextField(
                        onChanged: (value) {
                          searchCityName = value;
                        },
                        controller: searchTextController,
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          hintText: "City",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Material(
                    child: MaterialButton(
                      color: Colors.deepPurple,
                      elevation: 5.0,
                      height: 58,
                      onPressed: () async {
                        dynamic weatherData =
                            await _weatherModel.getCityWeather(searchCityName);
                        setState(() {
                          updateUI(weatherData);
                        });
                        searchTextController.clear();
                      },
                      child: Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NetworkHelper {
  NetworkHelper({this.url});
  final String url;

  Future getData() async {
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print(response.statusCode);
    }
  }
}

class Location {
  double latitude;
  double longitude;

  Future<void> getCurrentLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low);
      latitude = position.latitude;
      longitude = position.longitude;
    } catch (e) {
      print(e);
    }
  }
}

class WeatherModel {
  Future<dynamic> getCityWeather(String cityName) async {
    NetworkHelper networkHelper = NetworkHelper(
        url:
            "https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$WEATHER_API_KEY&units=metric");
    var weatherData = await networkHelper.getData();
    return weatherData;
  }

  Future<dynamic> getLocationWeather() async {
    Location location = Location();
    await location.getCurrentLocation();
    NetworkHelper networkHelper = NetworkHelper(
        url:
            "https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longitude}&appid=$WEATHER_API_KEY&units=metric");
    var weatherData = await networkHelper.getData();
    return weatherData;
  }

  String calculateTime(int unixTime) {
    var date = DateTime.fromMillisecondsSinceEpoch(unixTime * 1000);
    var format = DateFormat.jm();
    var time = format.format(date);
    return time;
  }

  IconData getWeatherIcon(int condition) {
    if (condition < 300) {
      return WeatherIcons.thunderstorm;
    } else if (condition < 400) {
      return WeatherIcons.showers;
    } else if (condition < 600) {
      return WeatherIcons.rain;
    } else if (condition < 700) {
      return WeatherIcons.snow;
    } else if (condition < 800) {
      return WeatherIcons.smoke;
    } else if (condition == 800) {
      return WeatherIcons.day_sunny;
    } else if (condition <= 804) {
      return WeatherIcons.cloud;
    } else {
      return WeatherIcons.thermometer_internal;
    }
  }
}
