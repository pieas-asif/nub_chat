import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatScreen extends StatefulWidget {
  static const String id = "chat_screen";
  // String conversationID;
  // ChatScreen({@required this.conversationID});
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  String messageText;
  var _loggedInUser;
  String _loggedInUserEmail;
  final senderController = TextEditingController();

  void getCurrentUser() {
    try {
      final _user = _auth.currentUser;
      if (_user != null) {
        _loggedInUser = _user;
        _loggedInUserEmail = _loggedInUser.email.toString();
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateStore(
      String conversationID, String message, int whoDis) async {
    await _fireStore.collection("conversations").doc(conversationID).update({
      "lastMessage": message,
      "read": whoDis == 0 ? [true, false] : [false, true],
      "time": Timestamp.now()
    });
  }

  Future<void> iHaveRead(String conversationID, int whoDis) async {
    DocumentSnapshot _docs;
    await _fireStore
        .collection("conversations")
        .doc(conversationID)
        .get()
        .then((value) => _docs = value);
    var readArray = _docs.get("read");
    whoDis == 0 ? readArray[0] = true : readArray[1] = true;
    await _fireStore.collection("conversations").doc(conversationID).update({
      "read": readArray,
    });
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    final Map conversationArgs = ModalRoute.of(context).settings.arguments;
    iHaveRead(conversationArgs['conversationID'], conversationArgs['whoDis']);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(conversationArgs["sender"]),
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            StreamBuilder<QuerySnapshot>(
              stream: _fireStore
                  .collection(
                      "conversations/${conversationArgs['conversationID']}/messages")
                  .orderBy("time")
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final messages = snapshot.data.docs.reversed;
                  List<MessageBubble> messagesWidgets = [];
                  for (var message in messages) {
                    final text = message.get("text");
                    final sender = message.get("sender");
                    final messageWidget = MessageBubble(
                      sender: sender,
                      text: text,
                      loggedInUserEmail: _loggedInUserEmail,
                    );
                    messagesWidgets.add(messageWidget);
                  }
                  return Expanded(
                    child: ListView(
                      reverse: true,
                      children: messagesWidgets,
                    ),
                  );
                } else {
                  return Text("No messages, Say Hi?");
                }
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: TextField(
                      onChanged: (value) {
                        messageText = value;
                      },
                      controller: senderController,
                      cursorColor: Colors.deepPurple,
                      decoration: InputDecoration(
                        hintText: "Write",
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 1.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.deepPurple.shade50,
                            width: 2.0,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.deepPurple,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Material(
                  child: MaterialButton(
                    color: Colors.deepPurple,
                    elevation: 5.0,
                    height: 58,
                    onPressed: () async {
                      await _fireStore
                          .collection(
                              "conversations/${conversationArgs['conversationID']}/messages")
                          .add({
                        "text": messageText,
                        "sender": _loggedInUser.email.toString(),
                        "time": Timestamp.now(),
                      });
                      updateStore(conversationArgs['conversationID'],
                          messageText, conversationArgs['whoDis']);
                      senderController.clear();
                      messageText = "";
                    },
                    child: Icon(
                      Icons.send,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                  height: 78,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  MessageBubble(
      {@required this.sender, @required this.text, this.loggedInUserEmail});

  Color color;
  Color textColor;
  final String loggedInUserEmail;
  final String sender;
  final String text;
  MainAxisAlignment rowAlignment;
  CrossAxisAlignment colAlignment;

  void getColor(String sender) {
    if (loggedInUserEmail == sender) {
      color = Colors.deepPurple;
      textColor = Colors.white;
      rowAlignment = MainAxisAlignment.end;
      colAlignment = CrossAxisAlignment.end;
    } else {
      color = Colors.deepPurple.shade50;
      textColor = Colors.black;
      rowAlignment = MainAxisAlignment.start;
      colAlignment = CrossAxisAlignment.start;
    }
  }

  @override
  Widget build(BuildContext context) {
    getColor(sender);
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 4.0,
        horizontal: 10.0,
      ),
      child: Row(
        mainAxisAlignment: rowAlignment,
        children: [
          Material(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2),
            ),
            color: color,
            child: Container(
              constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.7,
              ),
              padding: EdgeInsets.all(4),
              child: Text(
                "$text",
                style: TextStyle(
                  color: textColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
