import 'package:flutter/material.dart';
import 'package:nub_chat/Screens/login_screen.dart';
import 'package:nub_chat/Screens/registration_screen.dart';

class WelcomeScreen extends StatefulWidget {
  static const String id = "welcome_screen";

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    animation = CurvedAnimation(
      parent: controller,
      curve: Curves.decelerate,
    );
    controller.forward();
    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Hero(
              tag: "icon",
              child: Container(
                child: Image.asset("assets/images/chat.png"),
                height: (MediaQuery.of(context).size.height * 0.3) *
                    animation.value,
              ),
            ),
            Text(
              "NUB Chat",
              style: TextStyle(
                color: Colors.deepPurple,
                fontSize: 42 * animation.value,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.1,
            ),
            Material(
              child: MaterialButton(
                color: Colors.deepPurple,
                elevation: 5.0,
                height: 42,
                minWidth: 200,
                onPressed: () {
                  Navigator.pushNamed(context, LoginScreen.id);
                },
                child: Text(
                  "Log In",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Material(
              child: MaterialButton(
                color: Colors.deepPurple.shade50,
                elevation: 5.0,
                height: 42,
                minWidth: 200,
                onPressed: () {
                  Navigator.pushNamed(context, RegistrationScreen.id);
                },
                child: Text("Sign Up"),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
              width: double.infinity,
            ),
          ],
        ),
      ),
    );
  }
}
