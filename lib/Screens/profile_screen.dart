import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:nub_chat/Utils/firebase_storage_service.dart';

class ProfileScreen extends StatefulWidget {
  static const String id = "profile_screen";
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  final _storage = FirebaseStorage.instance;
  final _picker = ImagePicker();
  Image _image;
  String _name = "";
  String _status = "";
  String newName;
  String newStatus;
  String docID;
  var _loggedInUser;
  String _loggedInUserEmail;
  bool _loading = false;

  void getCurrentUser() {
    try {
      final _user = _auth.currentUser;
      if (_user != null) {
        _loggedInUser = _user;
        _loggedInUserEmail = _loggedInUser.email.toString();
        var _id = _loggedInUserEmail.split("@");
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> getUserInfo() async {
    List<QueryDocumentSnapshot> _info;
    await _fireStore
        .collection("users")
        .where("userID", isEqualTo: _loggedInUserEmail)
        .get()
        .then((value) => {
              _info = value.docs,
            })
        .catchError((e) => {
              CoolAlert.show(
                context: context,
                type: CoolAlertType.error,
                title: "Failed to get user data",
                text: "Error: $e",
                backgroundColor: Colors.deepPurple.shade50,
                confirmBtnColor: Colors.deepPurple,
              )
            });
    if (_info != null) {
      updateUI(_info[0].get("name"), _info[0].get("status"), _info[0].id,
          await _getImage(_info[0].get("image")));
    }
  }

  void updateUI(String userName, String sts, String id, Image image) {
    if (this.mounted) {
      setState(() {
        _image = image;
        _name = userName;
        _status = sts;
        docID = id;
      });
    }
  }

  Future<void> uploadImageToFirebase(File _img) async {
    String fileName = DateTime.now().toString();
    UploadTask _task = _storage.ref('avatars/$fileName').putFile(_img);

    try {
      await _task;
      await _fireStore
          .collection("users")
          .doc(docID)
          .update({
            "image": "avatars/$fileName",
          })
          .whenComplete(
            () => CoolAlert.show(
              context: context,
              type: CoolAlertType.success,
              title: "Picture Updated",
              text:
                  "Your profile Picture is successfully updated. Tap back button to go to Main Screen",
              backgroundColor: Colors.deepPurple.shade50,
              confirmBtnColor: Colors.deepPurple,
            ),
          )
          .catchError(
            (e) => {
              CoolAlert.show(
                context: context,
                type: CoolAlertType.error,
                title: "Something Went Wrong",
                text: "Error: $e",
                backgroundColor: Colors.deepPurple.shade50,
                confirmBtnColor: Colors.deepPurple,
              )
            },
          );
    } on FirebaseException catch (e) {
      if (e.code == 'permission-denied') {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          title: "Permission Denied",
          text: "You don't have the permission to upload file. Try Sudo?",
          backgroundColor: Colors.deepPurple.shade50,
          confirmBtnColor: Colors.deepPurple,
        );
      } else {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          title: "Something went wrong",
          text: "Error: $e",
          backgroundColor: Colors.deepPurple.shade50,
          confirmBtnColor: Colors.deepPurple,
        );
      }
    }
  }

  Future<Widget> _getImage(String imagePath) async {
    Image image;
    if (imagePath != null && imagePath != "") {
      await FirebaseStorageService.loadImage(imagePath).then((value) => {
            if (value != null)
              {
                image = Image.network(
                  value.toString(),
                  fit: BoxFit.fill,
                )
              }
          });
    }
    return image;
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    getUserInfo();
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Edit Profile"),
      ),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.1,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: CircleAvatar(
                        maxRadius: 120,
                        backgroundColor: Colors.deepPurple,
                        child: FlatButton(
                          padding: EdgeInsets.zero,
                          onPressed: () async {
                            try {
                              var pickedFile = await _picker.getImage(
                                  source: ImageSource.gallery);
                              if (pickedFile != null) {
                                if (File(pickedFile.path).lengthSync() >
                                    (2 * 1000000)) {
                                  CoolAlert.show(
                                    context: context,
                                    type: CoolAlertType.error,
                                    title: "File Size Limit Exceed",
                                    text: "Please choose a photo under 2MB",
                                    backgroundColor: Colors.deepPurple.shade50,
                                    confirmBtnColor: Colors.deepPurple,
                                  );
                                } else {
                                  File cropped = await ImageCropper.cropImage(
                                    sourcePath: pickedFile.path,
                                    maxWidth: 1000,
                                    maxHeight: 1000,
                                    aspectRatio: CropAspectRatio(
                                      ratioX: 1,
                                      ratioY: 1,
                                    ),
                                  );
                                  await uploadImageToFirebase(cropped);
                                  setState(() {
                                    _image = Image.file(
                                      File(cropped.path),
                                      fit: BoxFit.fill,
                                    );
                                  });
                                }
                              } else {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "No File Chosen",
                                  text: "Please select a File",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                );
                              }
                            } catch (e) {
                              CoolAlert.show(
                                context: context,
                                type: CoolAlertType.error,
                                title: "Something Went Vuul diik",
                                text: "Error $e",
                                backgroundColor: Colors.deepPurple.shade50,
                                confirmBtnColor: Colors.deepPurple,
                              );
                            }
                          },
                          onLongPress: () async {
                            try {
                              var pickedFile = await _picker.getImage(
                                  source: ImageSource.camera);
                              if (pickedFile != null) {
                                if (File(pickedFile.path).lengthSync() >
                                    (2 * 1000000)) {
                                  CoolAlert.show(
                                    context: context,
                                    type: CoolAlertType.error,
                                    title: "File Size Limit Exceed",
                                    text: "Please choose a photo under 2MB",
                                    backgroundColor: Colors.deepPurple.shade50,
                                    confirmBtnColor: Colors.deepPurple,
                                  );
                                } else {
                                  File cropped = await ImageCropper.cropImage(
                                    sourcePath: pickedFile.path,
                                    maxWidth: 1000,
                                    maxHeight: 1000,
                                    aspectRatio: CropAspectRatio(
                                      ratioX: 1,
                                      ratioY: 1,
                                    ),
                                  );
                                  await uploadImageToFirebase(cropped);
                                  setState(() {
                                    _image = Image.file(
                                      File(cropped.path),
                                      fit: BoxFit.fill,
                                    );
                                  });
                                }
                              } else {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "No File Chosen",
                                  text: "Please select a File",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                );
                              }
                            } catch (e) {
                              CoolAlert.show(
                                context: context,
                                type: CoolAlertType.error,
                                title: "Something Went Vuul diik",
                                text: "Error $e",
                                backgroundColor: Colors.deepPurple.shade50,
                                confirmBtnColor: Colors.deepPurple,
                              );
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: ClipOval(
                              child: _image == null
                                  ? Image.asset(
                                      "assets/images/avatar/avatar.png",
                                      fit: BoxFit.fill,
                                    )
                                  : _image,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 10,
                      ),
                      child: TextField(
                        onChanged: (value) {
                          newName = value;
                        },
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          labelText: _name,
                          hintText: "Enter new Name",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 10,
                      ),
                      child: TextField(
                        onChanged: (value) {
                          newStatus = value;
                        },
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          labelText: _status,
                          hintText: "Enter new Status",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Material(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: MaterialButton(
                  color: Colors.deepPurple,
                  elevation: 5.0,
                  height: 42,
                  minWidth: double.infinity,
                  onPressed: () async {
                    if ((newName == "" || newName == null) &&
                        (newStatus == "" || newStatus == null)) {
                      setState(() {
                        _loading = false;
                      });
                      CoolAlert.show(
                        context: context,
                        type: CoolAlertType.error,
                        title: "Blank Name or Status",
                        text: "Name and Status cannot be Blank",
                        backgroundColor: Colors.deepPurple.shade50,
                        confirmBtnColor: Colors.deepPurple,
                      );
                    } else if (newStatus == "" || newStatus == null) {
                      await _fireStore
                          .collection("users")
                          .doc(docID)
                          .update({
                            "name": newName,
                          })
                          .whenComplete(
                            () => CoolAlert.show(
                              context: context,
                              type: CoolAlertType.success,
                              title: "Profile Updated",
                              text:
                                  "Your profile successfully updated. Tap back button to go to Main Screen",
                              backgroundColor: Colors.deepPurple.shade50,
                              confirmBtnColor: Colors.deepPurple,
                            ),
                          )
                          .catchError(
                            (e) => {
                              CoolAlert.show(
                                context: context,
                                type: CoolAlertType.error,
                                title: "Something Went Wrong",
                                text: "Error: $e",
                                backgroundColor: Colors.deepPurple.shade50,
                                confirmBtnColor: Colors.deepPurple,
                              )
                            },
                          );
                    } else if (newName == "" || newName == null) {
                      if (newName.length > 30) {
                        CoolAlert.show(
                          context: context,
                          type: CoolAlertType.error,
                          title: "Name Limit Exceeded",
                          text: "Name must be within 30 letters",
                          backgroundColor: Colors.deepPurple.shade50,
                          confirmBtnColor: Colors.deepPurple,
                        );
                      } else {
                        await _fireStore
                            .collection("users")
                            .doc(docID)
                            .update({
                              "status": newStatus,
                            })
                            .whenComplete(
                              () => CoolAlert.show(
                                context: context,
                                type: CoolAlertType.success,
                                title: "Profile Updated",
                                text:
                                    "Your profile successfully updated. Tap back button to go to Main Screen",
                                backgroundColor: Colors.deepPurple.shade50,
                                confirmBtnColor: Colors.deepPurple,
                              ),
                            )
                            .catchError(
                              (e) => {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "Something Went Wrong",
                                  text: "Error: $e",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                )
                              },
                            );
                      }
                    } else {
                      await _fireStore
                          .collection("users")
                          .doc(docID)
                          .update({
                            "name": newName,
                            "status": newStatus,
                          })
                          .whenComplete(
                            () => CoolAlert.show(
                              context: context,
                              type: CoolAlertType.success,
                              title: "Profile Updated",
                              text:
                                  "Your profile successfully updated. Tap back button to go to Main Screen",
                              backgroundColor: Colors.deepPurple.shade50,
                              confirmBtnColor: Colors.deepPurple,
                            ),
                          )
                          .catchError(
                            (e) => {
                              CoolAlert.show(
                                context: context,
                                type: CoolAlertType.error,
                                title: "Something Went Wrong",
                                text: "Error: $e",
                                backgroundColor: Colors.deepPurple.shade50,
                                confirmBtnColor: Colors.deepPurple,
                              )
                            },
                          );
                    }
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
