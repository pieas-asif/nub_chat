import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:nub_chat/Screens/main_screen.dart';
import 'package:nub_chat/Utils/upper_case_text_formatter.dart';

class RegistrationScreen extends StatefulWidget {
  static const String id = "registration_screen";

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  bool _loading = false;
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  String _id;
  String _password;
  final nubIdRegEx = RegExp(r"^nub\d{9}$");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: FlatButton(
          padding: EdgeInsets.zero,
          onPressed: () => Navigator.pop(context),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              RichText(
                text: TextSpan(
                  text: "Back to",
                  style: TextStyle(color: Colors.white),
                  children: [
                    TextSpan(
                      text: "\nWelcome Screen",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        centerTitle: false,
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _loading,
          color: Colors.deepPurple,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.1,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    TypewriterAnimatedTextKit(
                      speed: Duration(milliseconds: 200),
                      text: ["SIGN UP"],
                      isRepeatingAnimation: false,
                      textStyle: TextStyle(
                        color: Colors.deepPurple,
                        fontSize: 36,
                      ),
                    ),
                  ],
                ),
                Hero(
                  tag: "icon",
                  child: Container(
                    child: Image.asset("assets/images/chat.png"),
                    height: MediaQuery.of(context).size.height * 0.4,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.1),
                      child: TextField(
                        onChanged: (value) {
                          _id = value;
                        },
                        textCapitalization: TextCapitalization.none,
                        inputFormatters: [
                          UpperCaseTextFormatter(),
                        ],
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          hintText: "Enter ID; e.g. NUB170101046",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.1),
                      child: TextField(
                        onChanged: (value) {
                          _password = value;
                        },
                        obscureText: true,
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          hintText: "Enter Password",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Material(
                      child: MaterialButton(
                        color: Colors.deepPurple,
                        elevation: 5.0,
                        height: 42,
                        minWidth: 200,
                        onPressed: () async {
                          setState(() {
                            _loading = true;
                          });
                          if (_id == null ||
                              _id == "" ||
                              _password == null ||
                              _password == "") {
                            setState(() {
                              _loading = false;
                            });
                            CoolAlert.show(
                              context: context,
                              type: CoolAlertType.error,
                              title: "Error",
                              text: "ID & Password fields cannot be empty",
                              backgroundColor: Colors.deepPurple.shade50,
                              confirmBtnColor: Colors.deepPurple,
                            );
                          } else if (!nubIdRegEx.hasMatch(_id.toLowerCase())) {
                            setState(() {
                              _loading = false;
                            });
                            CoolAlert.show(
                              context: context,
                              type: CoolAlertType.error,
                              title: "Invalid ID",
                              text: "Enter a Valid NUB ID. e.g. NUB170101046",
                              backgroundColor: Colors.deepPurple.shade50,
                              confirmBtnColor: Colors.deepPurple,
                            );
                          } else {
                            try {
                              final _newUser =
                                  await _auth.createUserWithEmailAndPassword(
                                      email: "${_id.toLowerCase()}@nub.ac.bd",
                                      password: _password);
                              if (_newUser != null) {
                                _fireStore.collection("users").add({
                                  "userID": "${_id.toLowerCase()}@nub.ac.bd",
                                  "name": "${_id.toUpperCase()}",
                                  "status": "Proud to be a NUBIAN",
                                  "image": "",
                                });
                                Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  MainScreen.id,
                                  (Route<dynamic> route) => false,
                                );
                              }
                              setState(() {
                                _loading = false;
                              });
                            } on FirebaseAuthException catch (e) {
                              setState(() {
                                _loading = false;
                              });
                              if (e.code == 'weak-password') {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "Weak Password",
                                  text:
                                      "Try setting up a more complex password",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                );
                              } else if (e.code == 'email-already-in-use') {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "User already in Database",
                                  text:
                                      "This ID is already associated with an user. Try logging in.",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                );
                              }
                            } catch (e) {
                              setState(() {
                                _loading = false;
                              });
                              CoolAlert.show(
                                context: context,
                                type: CoolAlertType.error,
                                title: "Unhandled Exception",
                                text: "Error: $e",
                                backgroundColor: Colors.deepPurple.shade50,
                                confirmBtnColor: Colors.deepPurple,
                              );
                            }
                          }
                        },
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
