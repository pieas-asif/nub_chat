import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:nub_chat/Screens/main_screen.dart';
import 'package:nub_chat/Utils/upper_case_text_formatter.dart';

class LoginScreen extends StatefulWidget {
  static const String id = "login_screen";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _loading = false;
  final _auth = FirebaseAuth.instance;
  String _id;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: FlatButton(
          padding: EdgeInsets.zero,
          onPressed: () => Navigator.pop(context),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              RichText(
                text: TextSpan(
                  text: "Back to",
                  style: TextStyle(color: Colors.white),
                  children: [
                    TextSpan(
                      text: "\nWelcome Screen",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        centerTitle: false,
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _loading,
          color: Colors.deepPurple,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.1,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    TypewriterAnimatedTextKit(
                      speed: Duration(milliseconds: 200),
                      text: ["LOG IN"],
                      isRepeatingAnimation: false,
                      textStyle: TextStyle(
                        color: Colors.deepPurple,
                        fontSize: 36,
                      ),
                    ),
                  ],
                ),
                Hero(
                  tag: "icon",
                  child: Container(
                    child: Image.asset("assets/images/chat.png"),
                    height: MediaQuery.of(context).size.height * 0.4,
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.1),
                      child: TextField(
                        onChanged: (value) {
                          _id = value;
                        },
                        textCapitalization: TextCapitalization.none,
                        inputFormatters: [
                          UpperCaseTextFormatter(),
                        ],
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          hintText: "Enter ID",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.1),
                      child: TextField(
                        onChanged: (value) {
                          _password = value;
                        },
                        obscureText: true,
                        cursorColor: Colors.deepPurple,
                        decoration: InputDecoration(
                          hintText: "Enter Password",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 1.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple.shade50,
                              width: 2.0,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.deepPurple,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Material(
                      child: MaterialButton(
                        color: Colors.deepPurple,
                        elevation: 5.0,
                        height: 42,
                        minWidth: 200,
                        onPressed: () async {
                          setState(() {
                            _loading = true;
                          });
                          if (_id == null ||
                              _id == "" ||
                              _password == null ||
                              _password == "") {
                            setState(() {
                              _loading = false;
                            });
                            CoolAlert.show(
                              context: context,
                              type: CoolAlertType.error,
                              title: "Error",
                              text: "ID & Password fields cannot be empty",
                              backgroundColor: Colors.deepPurple.shade50,
                              confirmBtnColor: Colors.deepPurple,
                            );
                          } else {
                            try {
                              final _user =
                                  await _auth.signInWithEmailAndPassword(
                                      email: "${_id.toLowerCase()}@nub.ac.bd",
                                      password: _password);
                              if (_user != null) {
                                Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  MainScreen.id,
                                  (Route<dynamic> route) => false,
                                );
                              }
                              setState(() {
                                _loading = false;
                              });
                            } on FirebaseAuthException catch (e) {
                              setState(() {
                                _loading = false;
                              });
                              if (e.code == 'user-not-found') {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "Unknown ID",
                                  text:
                                      "No user is associated with this ID, try signing up.",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                );
                              } else if (e.code == 'wrong-password') {
                                CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: "Wrong Password",
                                  text:
                                      "If you forgot your password, try contacting Admin for resetting the password.",
                                  backgroundColor: Colors.deepPurple.shade50,
                                  confirmBtnColor: Colors.deepPurple,
                                );
                              }
                            }
                          }
                        },
                        child: Text(
                          "Log In",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
