import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:nub_chat/Screens/profile_screen.dart';
import 'package:nub_chat/Screens/user_screen.dart';
import 'package:nub_chat/Screens/welcome_screen.dart';
import 'package:nub_chat/Utils/conversation_list.dart';

class MainScreen extends StatefulWidget {
  static const String id = "main_screen";
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  String messageText;
  var _loggedInUser;
  String _loggedInUserEmail;
  final senderController = TextEditingController();
  List<Widget> noConversations = [
    Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
      child: Center(
        child: Text(
          "No Conversations. To Start a conversation, tap on the write button.",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
    ),
  ];

  void getCurrentUser() {
    try {
      final _user = _auth.currentUser;
      if (_user != null) {
        _loggedInUser = _user;
        _loggedInUserEmail = _loggedInUser.email.toString();
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("NUB Chat"),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.account_circle,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, ProfileScreen.id);
                  },
                ),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    _auth.signOut().whenComplete(() =>
                        Navigator.pushNamedAndRemoveUntil(
                            context, WelcomeScreen.id, (route) => false));
                  },
                ),
              ],
            )
          ],
        ),
        centerTitle: false,
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 16, left: 16, right: 16),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Search",
                  hintStyle: TextStyle(color: Colors.black),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.black,
                    size: 20,
                  ),
                  filled: true,
                  fillColor: Colors.grey.shade100,
                  contentPadding: EdgeInsets.all(8),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                ),
                cursorColor: Colors.deepPurple,
              ),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: _fireStore
                  .collection("conversations")
                  .orderBy("time")
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final conversations = snapshot.data.docs.reversed;
                  List<ConversationList> conversationList = [];
                  for (var conversation in conversations) {
                    var people = conversation.get("people");
                    if ((people[0] == _loggedInUserEmail ||
                            people[1] == _loggedInUserEmail) &&
                        conversation.get("lastMessage") != "") {
                      final docID = conversation.id;
                      var people = conversation.get("people");
                      final receiverIter =
                          people[0] == _loggedInUserEmail ? 1 : 0;
                      final receiver = people[receiverIter];
                      final lastMessage = conversation.get("lastMessage");
                      final time = conversation.get("time");
                      final read = conversation.get("read");
                      final isRead = read[receiverIter];
                      final newConversation = ConversationList(
                        conversationID: docID,
                        userID: receiver.toString(),
                        messageText: lastMessage,
                        time: time,
                        isRead: isRead,
                        who: receiverIter,
                      );
                      conversationList.add(newConversation);
                    }
                  }
                  return Expanded(
                    child: ListView(
                      children: conversationList.isEmpty
                          ? noConversations
                          : conversationList,
                    ),
                  );
                } else {
                  return Center(
                    child: Text("Slow Retrieve or Could not Authenticate"),
                  );
                }
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, UserScreen.id);
        },
        child: Icon(Icons.create),
        backgroundColor: Colors.deepPurple,
      ),
    );
  }
}
