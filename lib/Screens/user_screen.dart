import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:nub_chat/Apps/weather_app.dart';
import 'package:nub_chat/Utils/app_list.dart';
import 'package:nub_chat/Utils/user_list.dart';

class UserScreen extends StatefulWidget {
  static const String id = "user_screen";
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  var _loggedInUser;
  String _loggedInUserEmail;
  List<Widget> appList = [
    AppList(
      routeID: WeatherApp.id,
      title: "Weather Man",
      subtitle: "Know the current forecast",
    ),
  ];

  void getCurrentUser() {
    try {
      final _user = _auth.currentUser;
      if (_user != null) {
        _loggedInUser = _user;
        _loggedInUserEmail = _loggedInUser.email.toString();
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Users"),
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 16, left: 16, right: 16),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Search",
                  hintStyle: TextStyle(color: Colors.black),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.black,
                    size: 20,
                  ),
                  filled: true,
                  fillColor: Colors.grey.shade100,
                  contentPadding: EdgeInsets.all(8),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                ),
                cursorColor: Colors.deepPurple,
              ),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: _fireStore
                  .collection("users")
                  .where("userID", isNotEqualTo: _loggedInUserEmail)
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final users = snapshot.data.docs;
                  List<UserList> userList = [];
                  for (var user in users) {
                    String userID = user.get("userID");
                    String userName = user.get("name");
                    String status = user.get("status");
                    String imagePath = user.get("image");
                    final newUser = UserList(
                      userID: userID,
                      userName: userName,
                      status: status,
                      imagePath: imagePath,
                    );
                    userList.add(newUser);
                  }
                  return Expanded(
                    child: ListView(
                      children: userList.isEmpty ? appList : appList + userList,
                    ),
                  );
                } else {
                  return Center(
                    child: Text("Slow Retrieve or Could not Authenticate"),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
