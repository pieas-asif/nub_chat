import 'package:flutter/material.dart';
import 'package:nub_chat/Apps/weather_app.dart';
import 'package:nub_chat/Screens/chat_screen.dart';
import 'package:nub_chat/Screens/login_screen.dart';
import 'package:nub_chat/Screens/main_screen.dart';
import 'package:nub_chat/Screens/profile_screen.dart';
import 'package:nub_chat/Screens/registration_screen.dart';
import 'package:nub_chat/Screens/user_screen.dart';
import 'package:nub_chat/Screens/welcome_screen.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'NUB Chat',
      theme: ThemeData(
        fontFamily: "Quicksand",
        primaryColor: Colors.deepPurple,
        accentColor: Colors.deepPurpleAccent,
        textSelectionColor: Colors.deepPurpleAccent,
        textSelectionHandleColor: Colors.deepPurple,
      ),
      initialRoute: WelcomeScreen.id,
      routes: {
        WelcomeScreen.id: (context) => WelcomeScreen(),
        LoginScreen.id: (context) => LoginScreen(),
        RegistrationScreen.id: (context) => RegistrationScreen(),
        ChatScreen.id: (context) => ChatScreen(),
        MainScreen.id: (context) => MainScreen(),
        UserScreen.id: (context) => UserScreen(),
        ProfileScreen.id: (context) => ProfileScreen(),
        // App Screens
        WeatherApp.id: (context) => WeatherApp(), // Weather application
      },
    );
  }
}
