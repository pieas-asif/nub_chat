import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:nub_chat/Screens/chat_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:time_formatter/time_formatter.dart';

import 'firebase_storage_service.dart';

class ConversationList extends StatefulWidget {
  String conversationID;
  String userID;
  String messageText;
  Timestamp time;
  bool isRead;
  final who;
  ConversationList(
      {@required this.conversationID,
      @required this.userID,
      @required this.messageText,
      this.time,
      this.isRead,
      this.who});
  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  String name;
  Image _image;

  void getCurrentUser() {
    try {
      final _user = _auth.currentUser;
      if (_user == null) {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          title: "Authentication Problem",
          text: "Something Went Wrong",
          backgroundColor: Colors.deepPurple.shade50,
          confirmBtnColor: Colors.deepPurple,
        );
      }
    } catch (e) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        title: "Authentication Problem",
        text: "Error: $e",
        backgroundColor: Colors.deepPurple.shade50,
        confirmBtnColor: Colors.deepPurple,
      );
    }
  }

  Future<Widget> _getImage(String imagePath) async {
    Image image;
    if (imagePath != null && imagePath != "") {
      await FirebaseStorageService.loadImage(imagePath).then((value) => {
            if (value != null)
              {
                image = Image.network(
                  value.toString(),
                  fit: BoxFit.fill,
                )
              }
          });
    }
    return image;
  }

  Future<void> getInfo() async {
    List<QueryDocumentSnapshot> info;
    await _fireStore
        .collection("users")
        .where("userID", isEqualTo: widget.userID)
        .get()
        .then((value) => info = value.docs);
    if (info.isNotEmpty) {
      getUserInfo(info[0].get("name"), await _getImage(info[0].get("image")));
    }
  }

  getUserInfo(String n, Image image) {
    setState(() {
      name = n;
      _image = image;
    });
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    getInfo();
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          ChatScreen.id,
          arguments: {
            "conversationID": widget.conversationID,
            "sender": name != null ? name : widget.userID,
            "whoDis": widget.who,
          },
        );
      },
      child: Container(
        padding: EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: _image != null
                        ? _image.image
                        : AssetImage("assets/images/avatar/avatar.png"),
                    maxRadius: 24,
                    backgroundColor: Colors.deepPurple,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            name != null ? name : widget.userID,
                            maxLines: 1,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: widget.isRead
                                  ? FontWeight.normal
                                  : FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            widget.messageText != null
                                ? widget.messageText
                                : "",
                            maxLines: 1,
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                              fontWeight: widget.isRead
                                  ? FontWeight.normal
                                  : FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Text(
              formatTime(widget.time.toDate().millisecondsSinceEpoch),
              style: TextStyle(
                fontSize: 12,
                fontWeight: widget.isRead ? FontWeight.normal : FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
