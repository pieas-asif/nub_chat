import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseStorageService extends ChangeNotifier {
  final _auth = FirebaseAuth.instance;

  FirebaseStorageService() {
    try {
      final _user = _auth.currentUser;
      if (_user != null) {
        print("SUCCESS");
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<dynamic> loadImage(String image) async {
    return await FirebaseStorage.instance.ref().child(image).getDownloadURL();
  }
}
