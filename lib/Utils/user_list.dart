import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:marquee/marquee.dart';
import 'package:nub_chat/Screens/chat_screen.dart';

import 'firebase_storage_service.dart';

class UserList extends StatefulWidget {
  String userID;
  String userName;
  String status;
  String imagePath;
  UserList(
      {@required this.userName,
      @required this.status,
      @required this.userID,
      this.imagePath});
  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  final _fireStore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  var _loggedInUser;
  String _loggedInUserEmail;
  Image _image;

  Future<String> getConversationID(String userID) async {
    String conversationID;
    List<QueryDocumentSnapshot> conversations;
    await _fireStore
        .collection("conversations")
        .where("people", arrayContains: userID)
        .get()
        .then((value) => {
              conversations = value.docs,
            });
    for (var conversation in conversations) {
      List<dynamic> peoples = conversation.get("people");
      if (peoples.contains(_loggedInUserEmail)) {
        conversationID = conversation.id;
      }
    }
    if (conversationID == null) {
      await _fireStore.collection("conversations").add({
        "people": [userID, _loggedInUserEmail],
        "lastMessage": "",
        "read": [true, true],
      }).then(
        (value) => conversationID = value.id,
      );
    }
    return conversationID;
  }

  void getCurrentUser() {
    try {
      final _user = _auth.currentUser;
      if (_user != null) {
        _loggedInUser = _user;
        _loggedInUserEmail = _loggedInUser.email.toString();
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> _getImage(String imagePath) async {
    if (imagePath != null && imagePath != "") {
      await FirebaseStorageService.loadImage(imagePath).then((value) => {
            if (value != null)
              {
                _image = Image.network(
                  value.toString(),
                  fit: BoxFit.fill,
                )
              }
          });
    }
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    _getImage(widget.imagePath);
    return GestureDetector(
      onTap: () async {
        String conversationID = await getConversationID(widget.userID);
        Navigator.pushNamed(
          context,
          ChatScreen.id,
          arguments: {
            "conversationID": conversationID,
            "sender": widget.userName,
          },
        );
      },
      child: Container(
        padding: EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: _image != null
                        ? _image.image
                        : AssetImage("assets/images/avatar/avatar.png"),
                    maxRadius: 24,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.userName,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          SizedBox(
                            height: 16,
                            child: widget.status.length > 30
                                ? Marquee(
                                    text: widget.status,
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    startPadding: 0,
                                    blankSpace: 200,
                                    startAfter: Duration(seconds: 5),
                                    pauseAfterRound: Duration(seconds: 5),
                                    numberOfRounds: 3,
                                  )
                                : Text(
                                    widget.status,
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
